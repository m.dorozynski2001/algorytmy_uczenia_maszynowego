from typing import Any

import tensorflow as tf  # type: ignore[import-untyped]
from pandas import DataFrame
from sklearn.calibration import LabelEncoder


def initialize_model_diabetes(X_train: DataFrame) -> tf.keras.models.Sequential:
    model = tf.keras.models.Sequential(
        [
            tf.keras.layers.Dense(128, activation="relu", input_shape=(X_train.shape[1],)),
            tf.keras.layers.Dense(64, activation="relu"),
            tf.keras.layers.Dense(32, activation="relu"),
            tf.keras.layers.Dense(1, activation="sigmoid"),
        ]
    )
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=["accuracy"])
    return model


def generate_data_diabetes(dt_k: DataFrame) -> tuple[Any, DataFrame]:
    # Przetwarzanie kolumn tekstowych za pomocą Label Encoding
    label_encoder = LabelEncoder()
    dt_k["gender"] = label_encoder.fit_transform(dt_k["gender"])
    dt_k["smoking_history"] = label_encoder.fit_transform(dt_k["smoking_history"])

    # Podział danych na cechy (X) i etykiety (y)
    Y = dt_k["diabetes"]
    X = dt_k.drop(columns=["diabetes"])
    return Y, X


def initialize_model_sloth(X_train: DataFrame) -> tf.keras.models.Sequential:
    model = tf.keras.models.Sequential(
        [
            tf.keras.layers.Dense(128, input_dim=X_train.shape[1], activation="relu"),
            tf.keras.layers.Dense(6, activation="sigmoid"),
        ]
    )
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    return model


def generate_data_sloth(dt_k: DataFrame) -> tuple[Any, DataFrame]:
    label_encoder = LabelEncoder()
    dt_k["endangered"] = label_encoder.fit_transform(dt_k["endangered"])
    dt_k["specie"] = label_encoder.fit_transform(dt_k["specie"])
    dt_k["sub_specie"] = label_encoder.fit_transform(dt_k["sub_specie"])

    # Podział danych na cechy (X) i etykiety (y)
    Y = dt_k["sub_specie"]
    X = dt_k.drop(columns=["sub_specie"])

    return Y, X
