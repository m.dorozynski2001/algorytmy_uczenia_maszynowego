from pathlib import Path

import pandas as pd
import tensorflow as tf  # type: ignore[import-untyped]
import typer
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

from Sloth_Code.common import (
    generate_data_diabetes,
    generate_data_sloth,
    initialize_model_diabetes,
    initialize_model_sloth,
)


def inner_main(
    csv_data: Path = typer.Argument(
        "diamond.csv",
        exists=True,
        file_okay=True,
        dir_okay=False,
        help="Path to folder containing training files.",
    ),
    weights: Path = typer.Option(..., exists=True, file_okay=True, dir_okay=False, help="Path to pretrained .h5 model"),
) -> None:

    # Wczytanie danych z pliku CSV
    dt_k = pd.read_csv(csv_data)
    # Wymieszanie wierszy danych
    dt_k = shuffle(dt_k, random_state=42)

    if csv_data.name == "sloth_data.csv":
        y, X = generate_data_sloth(dt_k)
        # Konwersja etykiet na postać kategorialną
        Y = tf.keras.utils.to_categorical(y)
    elif csv_data.name == "diabetes_data.csv":
        Y, X = generate_data_diabetes(dt_k)

    # Podział danych na zbiór treningowy, walidacyjny i testowy
    X_train, X_temp, Y_train, Y_temp = train_test_split(X, Y, test_size=0.4, random_state=42)
    X_val, X_test, Y_val, y_test = train_test_split(X_temp, Y_temp, test_size=0.5, random_state=42)
    if csv_data.name == "sloth_data.csv":
        model = initialize_model_sloth(X_train)
    elif csv_data.name == "diabetes_data.csv":
        model = initialize_model_diabetes(X_train)

    model.load_weights(weights)

    output = model.predict(X_test)

    # Wyświetlenie wyjścia modelu
    print(y_test)
    print("Wyjście modelu:")
    for out, Y in zip(output, y_test):
        print(f"{int(out.round())} {Y}")

    val_loss, val_accuracy = model.evaluate(X_test, y_test)
    # val_times.append(train_time)

    print("Dokładność na zbiorze testowym:", "{:.2%}".format(val_accuracy))
    print("Loss na zbiorze testowym:", "{:.2%}".format(val_loss))


def main() -> None:
    typer.run(inner_main)


if __name__ == "__main__":
    main()
