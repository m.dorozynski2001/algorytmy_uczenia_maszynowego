import time
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd
import tensorflow as tf  # type: ignore[import-untyped]
import typer
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

from Sloth_Code.common import (
    generate_data_diabetes,
    generate_data_sloth,
    initialize_model_diabetes,
    initialize_model_sloth,
)


def inner_main(
    csv_data: Path = typer.Argument(
        "sloth_data.csv",
        exists=True,
        file_okay=True,
        dir_okay=False,
        help="Path to folder containing training files.",
    ),
    verbose: int = typer.Option(
        0,
        help="Verbose value",
    ),
    output_dir: Path = typer.Option(
        "training",
        dir_okay=True,
        file_okay=False,
        help="Path to teaching output folder.",
    ),
    weights: Path = typer.Option(None, dir_okay=False, file_okay=True, help="Path to pretrained .h5 model"),
) -> None:
    current_date = str(datetime.now())

    # Wczytanie danych z pliku CSV
    dt_k = pd.read_csv(csv_data)

    # Wymieszanie wierszy danych
    dt_k = shuffle(dt_k, random_state=42)

    if csv_data.name == "sloth_data.csv":
        y, X = generate_data_sloth(dt_k)
        # Konwersja etykiet na postać kategorialną
        Y = tf.keras.utils.to_categorical(y)
    elif csv_data.name == "diabetes_data.csv":
        Y, X = generate_data_diabetes(dt_k)

    # Podział danych na zbiór treningowy, walidacyjny i testowy
    X_train, X_temp, Y_train, Y_temp = train_test_split(X, Y, test_size=0.4, random_state=42)
    X_val, Y_test, y_val, Y_test = train_test_split(X_temp, Y_temp, test_size=0.1, random_state=42)

    if csv_data.name == "sloth_data.csv":
        model = initialize_model_sloth(X_train)
    elif csv_data.name == "diabetes_data.csv":
        model = initialize_model_diabetes(X_train)

    # Liczby epok do przetestowania
    num_epochs = [10, 50, 100, 1000, 5000, 10000]
    (
        train_losses,
        val_losses,
        train_accuracies,
        val_accuracies,
        train_times,
    ) = ([], [], [], [], [])

    for epochs in num_epochs:
        checkpoint_path = output_dir / f"{csv_data.name}_{current_date}" / str(epochs)

        Path(checkpoint_path).mkdir(exist_ok=True, parents=True)
        print(f"\nTrenowanie modelu dla liczby epok: {epochs}")

        cp_callback = tf.keras.callbacks.ModelCheckpoint(
            filepath=f"{checkpoint_path}/training.weights.h5",
            save_weights_only=True,
            verbose=verbose,
        )
        log_file = f"{checkpoint_path}/log.csv"
        csv_logger = tf.keras.callbacks.CSVLogger(filename=log_file, separator=",", append=True)

        if weights is not None:
            model.load_weights(weights)

        # Pomiar czasu
        start_time = time.time()

        # Trenowanie modelu
        _ = model.fit(
            X_train,
            Y_train,
            batch_size=32,
            epochs=epochs,
            validation_data=(X_val, y_val),
            callbacks=[cp_callback, csv_logger],
        )

        # Pomiar czasu
        end_time = time.time()
        train_time = end_time - start_time
        print(f"Czas trenowania: {train_time:.2f} sekund")
        train_times.append(train_time)

        # Ocena modelu na zbiorze treningowym
        train_loss, train_accuracy = model.evaluate(X_train, Y_train)
        train_losses.append(train_loss)
        train_accuracies.append(train_accuracy)

        # Ocena modelu na zbiorze walidacyjnym
        val_loss, val_accuracy = model.evaluate(X_val, y_val)
        val_losses.append(val_loss)
        val_accuracies.append(val_accuracy)
        # val_times.append(train_time)

        print("Dokładność na zbiorze treningowym:", "{:.2%}".format(train_accuracy))
        print("Dokładność na zbiorze walidacyjnym:", "{:.2%}".format(val_accuracy))

    # Wykresy
    plt.figure(figsize=(10, 5))

    # Dokładność treningowa i walidacyjna
    plt.subplot(1, 2, 1)
    plt.plot(num_epochs, train_accuracies, label="Dokładność treningowa", marker="o")
    plt.plot(num_epochs, val_accuracies, label="Dokładność walidacyjna", marker="o")
    plt.title("Dokładność treningowa i walidacyjna")
    plt.xlabel("Liczba epok")
    plt.ylabel("Dokładność")
    plt.legend()
    plt.grid(True)

    # Czas trenowania
    plt.subplot(1, 2, 2)
    plt.plot(num_epochs, train_times, label="Czas trenowania", marker="o")
    plt.title("Czas trenowania")
    plt.xlabel("Liczba epok")
    plt.ylabel("Czas (s)")
    plt.legend()
    plt.grid(True)

    plt.tight_layout()
    plt.show()


def main() -> None:
    typer.run(inner_main)


if __name__ == "__main__":
    main()
