import random
from datetime import datetime
from pathlib import Path

import numpy as np
import typer
from numpy.typing import NDArray


def save_to_file(
    inputs: NDArray[np.int_],
    expected_output: NDArray[np.int_],
    split_name: str,
    output_dir: Path,
    samples_amout: int,
) -> None:
    current_date = datetime.now().date()
    output_path = output_dir / f"{current_date}.{samples_amout}_samples.{split_name}"
    output_path.mkdir(exist_ok=True, parents=True)
    np.savetxt(f"{output_path}/inputs.txt", inputs)
    np.savetxt(
        f"{output_path}/expected_output.txt",
        expected_output,
    )
    print(f"Dataset {split_name} saved in {output_path}")


def inner_main(
    samples_amount: int = typer.Argument(
        None,
        help="Number of generated learning samples.",
    ),
    split_name: str = typer.Option(
        "train",
        help="Split name.",
    ),
    output_dir: Path = typer.Option(
        "XOR_data",
        dir_okay=True,
        file_okay=False,
        exists=False,
        help="Output dir of file",
    ),
) -> None:

    if samples_amount is not None:
        sample_amount_divided = int(samples_amount / 4)
        inputs_1 = np.array([(random.randint(0, 4), random.randint(5, 9)) for _ in range(sample_amount_divided)])
        inputs_2 = np.array([(random.randint(5, 9), random.randint(0, 4)) for _ in range(sample_amount_divided)])
        inputs_3 = np.array([(random.randint(0, 4), random.randint(0, 4)) for _ in range(sample_amount_divided)])
        inputs_4 = np.array([(random.randint(5, 9), random.randint(5, 9)) for _ in range(sample_amount_divided)])
        inputs = np.concatenate((inputs_1, inputs_2, inputs_3, inputs_4), axis=0)
    else:
        inputs = np.array([(i, j) for i in range(10) for j in range(10)])  # type: ignore[unreachable]

    positive_counter = 0
    negative_counter = 0
    expected_output = np.zeros(len(inputs), dtype=int)
    for i, (a, b) in enumerate(inputs):
        if a > 4 and b < 5:
            expected_output[i] = 1
            positive_counter += 1
        elif a < 5 and b > 4:
            expected_output[i] = 1
            positive_counter += 1
        else:
            negative_counter += 1

    print(f"Generated:\npositive: {positive_counter}\nnegative: {negative_counter}")

    save_to_file(inputs, expected_output, split_name, output_dir, samples_amount)


def main() -> None:
    typer.run(inner_main)


if __name__ == "__main__":
    main()
